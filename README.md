# Notification_service

1. Run docker compose.
```
docker-compose up --build 
```

or

```
docker compose up --build 
```

2. Open the main main page (you can change port in the settings if you need)
```
http://localhost:8001/ (it will automatically redirect on http://localhost:8001/openapi/ - the instruction for api service using Swagger UI)
```

3. You can you use Postman by copying the openapi.json in it or use the default basic root view for DefaultRouter - the page available on 
```
http://localhost:8001/api/
```

4. Test endpoints :)

5. Доп. задания, которые удалось выполнить:
```
1. Организовать тестирование написанного кода - python3 notification_service/manage.py test notifications (Убедитесь, что установлены requirements.txt - pip install -r requirements.txt). 
Тесты находятся в файле notification_service/notifications/test.py

3. Подготовить docker-compose для запуска всех сервисов проекта одной командой

5. Сделать так, чтобы по адресу /docs/ открывалась страница со Swagger UI и в нём отображалось описание разработанного API. Пример: https://petstore.swagger.io
```
