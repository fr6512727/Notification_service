from django.contrib import admin

from .models import Client, Dispatch, Message


@admin.register(Dispatch)
class DispatchAdmin(admin.ModelAdmin):
    pass

@admin.register(Client)
class ClientAdmin(admin.ModelAdmin):
    pass

@admin.register(Message)
class MessageAdmin(admin.ModelAdmin):
    pass
