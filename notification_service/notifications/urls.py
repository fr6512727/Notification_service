from django.urls import include, path
from rest_framework import routers

from .views import ClientViewSet, DispatchViewSet, MessageViewSet

router = routers.DefaultRouter()
router.register('dispatches', DispatchViewSet)
router.register('clients', ClientViewSet)
router.register('messages', MessageViewSet)

urlpatterns = [
    path('', include(router.urls)),
]
