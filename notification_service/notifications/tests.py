from django.test import TestCase
from rest_framework import status
from rest_framework.test import APIClient

from .models import Client, Dispatch, Message


class APITestCase(TestCase):
    def setUp(self):
        self.client = APIClient()
        self.client_1 = Client.objects.create(
            phone_number='1234567890',
            operator_code='OP1',
            tag='TAG1',
            timezone='UTC'
        )
        self.client_2 = Client.objects.create(
            phone_number='9876543210',
            operator_code='OP2',
            tag='TAG2',
            timezone='GMT'
        )
        self.dispatch = Dispatch.objects.create(
            start_time='2023-07-01T10:00:00Z',
            end_time='2023-07-01T12:00:00Z',
            message='Test message',
            operator_code='OP1',
            tag='TAG1'
        )
        self.client_3 = Client.objects.create(
            phone_number='1111111111',
            operator_code='OP1',
            tag='TAG1',
            timezone='UTC'
        )
        self.client_4 = Client.objects.create(
            phone_number='2222222222',
            operator_code='OP1',
            tag='TAG1',
            timezone='UTC'
        )

    def test_create_client(self):
        url = '/api/clients/'
        data = {
            'phone_number': '5555555555',
            'operator_code': 'OP3',
            'tag': 'TAG3',
            'timezone': 'PST'
        }
        response = self.client.post(url, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(Client.objects.count(), 5)


    def test_update_client(self):
        url = f'/api/clients/{self.client_1.pk}/'
        data = {
            'phone_number': '1234567890',
            'operator_code': 'OP3',
            'tag': 'TAG3',
            'timezone': 'PST'
        }
        response = self.client.put(url, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(Client.objects.get(pk=self.client_1.pk).operator_code, 'OP3')


    def test_delete_client(self):
        url = f'/api/clients/{self.client_1.pk}/'
        response = self.client.delete(url)
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)
        self.assertEqual(Client.objects.count(), 3)


    def test_create_dispatch(self):
        url = '/api/dispatches/'
        data = {
            'start_time': '2023-07-01T10:00:00Z',
            'end_time': '2023-07-01T12:00:00Z',
            'message': 'Test dispatch',
            'operator_code': 'OP2',
            'tag': 'TAG2'
        }
        response = self.client.post(url, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(Dispatch.objects.count(), 2)


    def test_send_messages(self):
        url = f'/api/dispatches/{self.dispatch.pk}/send_messages/'
        response = self.client.post(url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(Message.objects.count(), 3)

    def test_update_message_delivery_status(self):
        message = Message.objects.first()
        if message:
            url = f'/api/messages/{message.pk}/delivery_status/'
            data = {
                'status': 'delivered'
            }
            response = self.client.post(url, data, format='json')
            self.assertEqual(response.status_code, status.HTTP_200_OK)
            self.assertEqual(Message.objects.get(pk=message.pk).status, 'delivered')
        else:
            print('No messages found')
