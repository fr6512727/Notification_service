import requests

from .models import Message


class NotificationService:
    BASE_URL = 'https://probe.fbrq.cloud/v1'

    #hide token in .env if you need
    @staticmethod
    def send_message(phone_number, message, message_id):
        url = f'{NotificationService.BASE_URL}/send/{message_id}'
        headers = {
            'Authorization': 'Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJleHAiOjE3MjAwMDk0MDEsImlzcyI6ImZhYnJpcXVlIiwibmFtZSI6Imh0dHBzOi8vdC5tZS9oYWhIRUhFSCJ9.PB_GCwWL5-Ya_Nd_ESvKCuqH2LXVc0DJihy5CL0pti0', 
            'Content-Type': 'application/json'
        }
        data = {
            'id': message_id,
            'phone': phone_number,
            'text': message
        }

        try:
            response = requests.post(url, headers=headers, json=data)
            response.raise_for_status()
        except requests.exceptions.RequestException as e:
            Message.objects.filter(id=message_id).update(status='failed')
        else:
            Message.objects.filter(id=message_id).update(status='sent')

    @staticmethod
    def handle_message_delivery_status(msg_id, status):
        Message.objects.filter(id=msg_id).update(status=status)
