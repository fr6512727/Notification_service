from django.db import models


class Dispatch(models.Model):
    start_time = models.DateTimeField()
    end_time = models.DateTimeField()
    message = models.TextField()
    operator_code = models.CharField(max_length=20)
    tag = models.CharField(max_length=50)


class Client(models.Model):
    phone_number = models.CharField(max_length=12, unique=True)
    operator_code = models.CharField(max_length=20)
    tag = models.CharField(max_length=50)
    timezone = models.CharField(max_length=50)


class Message(models.Model):
    created_at = models.DateTimeField(auto_now_add=True)
    status = models.CharField(max_length=50)
    dispatch = models.ForeignKey(Dispatch, on_delete=models.CASCADE, related_name='messages')
    client = models.ForeignKey(Client, on_delete=models.CASCADE)
