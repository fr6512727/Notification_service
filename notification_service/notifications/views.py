from rest_framework import viewsets
from rest_framework.decorators import action
from rest_framework.response import Response

from .models import Client, Dispatch, Message
from .serializers import (ClientSerializer, DispatchSerializer,
                          MessageSerializer)
from .services import NotificationService


class DispatchViewSet(viewsets.ModelViewSet):
    queryset = Dispatch.objects.all()
    serializer_class = DispatchSerializer

    @action(detail=True, methods=['post'])
    def send_messages(self, request, pk=None):
        dispatch = self.get_object()
        clients = Client.objects.filter(operator_code=dispatch.operator_code, tag=dispatch.tag)
        
        for client in clients:
            message = Message.objects.create(
                dispatch=dispatch,
                client=client,
                status='pending'
            )
            NotificationService.send_message(client.phone_number, dispatch.message, message.id)

        return Response({'status': 'Messages sent successfully.'})


class ClientViewSet(viewsets.ModelViewSet):
    queryset = Client.objects.all()
    serializer_class = ClientSerializer


class MessageViewSet(viewsets.ModelViewSet):
    queryset = Message.objects.all()
    serializer_class = MessageSerializer
