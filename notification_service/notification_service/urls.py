from django.contrib import admin
from django.urls import include, path, reverse_lazy
from django.views.generic import TemplateView
from django.views.generic.base import RedirectView

urlpatterns = [
    path('admin/', admin.site.urls),
    path('api/', include('notifications.urls')),
    path('', RedirectView.as_view(url=reverse_lazy('openapi'), permanent=False)),
    path('openapi/', TemplateView.as_view(template_name='openapi.html'), name='openapi'),
]
