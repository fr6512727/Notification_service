import json

# Define the OpenAPI specification as a dictionary
openapi_spec = {
  "openapi": "3.0.0",
  "info": {
    "title": "Сервис уведомлений API",
    "version": "1.0.0"
  },
  "paths": {
    "/api/clients/": {
      "post": {
        "summary": "Создание клиента",
        "requestBody": {
          "required": True,
          "content": {
            "application/json": {
              "schema": {
                "$ref": "#/components/schemas/Client"
              }
            }
          }
        },
        "responses": {
          "201": {
            "description": "Клиент успешно создан",
            "content": {
              "application/json": {
                "schema": {
                  "$ref": "#/components/schemas/Client"
                }
              }
            }
          }
        }
      }
    },
    "/api/clients/{client_id}/": {
      "put": {
        "summary": "Обновление данных клиента",
        "parameters": [
          {
            "in": "path",
            "name": "client_id",
            "schema": {
              "type": "integer"
            },
            "required": True
          }
        ],
        "requestBody": {
          "required": True,
          "content": {
            "application/json": {
              "schema": {
                "$ref": "#/components/schemas/Client"
              }
            }
          }
        },
        "responses": {
          "200": {
            "description": "Данные клиента успешно обновлены",
            "content": {
              "application/json": {
                "schema": {
                  "$ref": "#/components/schemas/Client"
                }
              }
            }
          }
        }
      },
      "delete": {
        "summary": "Удаление клиента",
        "parameters": [
          {
            "in": "path",
            "name": "client_id",
            "schema": {
              "type": "integer"
            },
            "required": True
          }
        ],
        "responses": {
          "204": {
            "description": "Клиент успешно удален"
          }
        }
      }
    },
    "/api/dispatches/": {
      "post": {
        "summary": "Создание рассылки",
        "requestBody": {
          "required": True,
          "content": {
            "application/json": {
              "schema": {
                "$ref": "#/components/schemas/Dispatch"
              }
            }
          }
        },
        "responses": {
          "201": {
            "description": "Рассылка успешно создана",
            "content": {
              "application/json": {
                "schema": {
                  "$ref": "#/components/schemas/Dispatch"
                }
              }
            }
          }
        }
      }
    },
    "/api/dispatches/{dispatch_id}/send_messages/": {
      "post": {
        "summary": "Отправка сообщений",
        "parameters": [
          {
            "in": "path",
            "name": "dispatch_id",
            "schema": {
              "type": "integer"
            },
            "required": True
          }
        ],
        "responses": {
          "200": {
            "description": "Сообщения успешно отправлены",
            "content": {
              "application/json": {
                "schema": {
                  "type": "object",
                  "properties": {
                    "status": {
                      "type": "string"
                    }
                  }
                }
              }
            }
          }
        }
      }
    },
    "/api/messages/{message_id}/delivery_status/": {
      "post": {
        "summary": "Обновление статуса доставки сообщения",
        "parameters": [
          {
            "in": "path",
            "name": "message_id",
            "schema": {
              "type": "integer"
            },
            "required": True
          }
        ],
        "requestBody": {
          "required": True,
          "content": {
            "application/json": {
              "schema": {
                "type": "object",
                "properties": {
                  "status": {
                    "type": "string"
                  }
                }
              }
            }
          }
        },
        "responses": {
          "200": {
            "description": "Статус доставки сообщения успешно обновлен"
          }
        }
      }
    }
  },
  "components": {
    "schemas": {
      "Client": {
        "type": "object",
        "properties": {
          "phone_number": {
            "type": "string"
          },
          "operator_code": {
            "type": "string"
          },
          "tag": {
            "type": "string"
          },
          "timezone": {
            "type": "string"
          }
        }
      },
      "Dispatch": {
        "type": "object",
        "properties": {
          "start_time": {
            "type": "string",
            "format": "date-time"
          },
          "end_time": {
            "type": "string",
            "format": "date-time"
          },
          "message": {
            "type": "string"
          },
          "operator_code": {
            "type": "string"
          },
          "tag": {
            "type": "string"
          }
        }
      }
    }
  }
}

# Convert the OpenAPI specification to JSON
openapi_json = json.dumps(openapi_spec)

# Save the JSON to a file
with open('openapi.json', 'w') as f:
    f.write(openapi_json)
